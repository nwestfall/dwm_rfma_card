<?php
$method = $_SERVER['REQUEST_METHOD'];
$servername = "localhost";
$username = "root";
$password = "dwm_rfma";
$dbname = "dwm";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

switch($method)
{
	case 'GET':
		getAction($conn);
		break;
	case 'POST':
		doPost($conn);
		break;
}

function getAction($conn)
{
	$sql = "SELECT leaderboard.userid, leaderboard.score, users.first, users.last, users.company FROM leaderboard INNER JOIN users ON leaderboard.userid = users.id ORDER BY leaderboard.score DESC LIMIT 5";
	$result = $conn->query($sql);

	if ($result->num_rows > 0) {
		// output data of each row
		while($row = $result->fetch_assoc()) {
		    echo "<li data-userid=" . $row["userid"] . ">" . $row["first"] . " " . $row["last"] . " from " . $row["company"] . " - " . $row["score"] . "</li>";
		}
	} else {
		echo "<li>No one yet!</li>";
	}
}

function doPost($conn)
{
	$userid = $_POST['userid'];
	$seconds = $_POST['time'];
	$clicks = $_POST['clicks'];
	$score = $_POST['score'];
	$sql = "INSERT INTO leaderboard (userid, seconds, clicks, score) VALUES ('$userid','$seconds','$clicks','$score')";
	if ($conn->query($sql) === TRUE) {
			$created = date("Y-m-d h:i:sa");
			$sql = "UPDATE users SET played = '$created' WHERE id = '$userid'";
			if ($conn->query($sql) === TRUE) {
				//YAY
			}
			getAction($conn);
	}
	else {
		echo "Error: " . $sql . "<br>" . $conn->error;
	}
}

$conn->close();