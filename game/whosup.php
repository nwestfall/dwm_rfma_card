<?php
$method = $_SERVER['REQUEST_METHOD'];
$servername = "localhost";
$username = "root";
$password = "dwm_rfma";
$dbname = "dwm";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

switch($method)
{
	case 'GET':
		getAction($conn);
		break;
	case 'POST':
		doPost($conn);
		break;
}

function getAction($conn)
{
	$type = $_GET['type'];
	switch($type)
	{
		case 'id':
			$sql = "SELECT upnext.userid FROM upnext WHERE upnext.status != 1 ORDER BY upnext.ordertime ASC LIMIT 1";
			$result = $conn->query($sql);
			if ($result->num_rows > 0) {
				// output data of each row
				while($row = $result->fetch_assoc()) {
				    echo $row["userid"];
				}
			}
			else
			{
				echo "N/A";
			}
			break;
		case 'name':
			$sql = "SELECT users.first, users.last FROM upnext INNER JOIN users ON upnext.userid = users.id WHERE upnext.status != 1 ORDER BY upnext.ordertime ASC LIMIT 1";
			$result = $conn->query($sql);
			if ($result->num_rows > 0) {
				// output data of each row
				while($row = $result->fetch_assoc()) {
				    echo $row["first"] . " " . $row["last"];
				}
			}
			else
			{
				echo "N/A";
			}
			break;
	}
}

function doPost($conn)
{
	$userid = $_POST['userid'];
	$sql = "UPDATE upnext SET upnext.status = 1 WHERE upnext.userid = '$userid'";
	$result = $conn->query($sql);
}

$conn->close();